
package com.example.recycler
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView

class Fragment1:Fragment(R.layout.fragment1_layout) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val profiles = listOf(
            Profile(
                image = R.drawable.boy1,
                Name = "David Borg",
                Title = "Flying Wings",
                popularity = "2342",
                likes = "4738",
                followed = "136",
                ranking = "1"
            ),
            Profile(
                image = R.drawable.g1,
                Name = "Lucy",
                Title = "Growing up trouble",
                popularity = "2342",
                likes = "4738",
                followed = "136",
                ranking = "2"
            ),
            Profile(
                image = R.drawable.b2,
                Name = "Jerry Cool West",
                Title = "Sculptors Diary",
                popularity = "2342",
                likes = "4738",
                followed = "136",
                ranking = "3"
            ),
            Profile(
                image = R.drawable.g2,
                Name = "Bold",
                Title = "Illustration of the little Girl",
                popularity = "2342",
                likes = "4738",
                followed = "136",
                ranking = "4"
            ),
            Profile(
                image = R.drawable.g3,
                Name = "Jerry Cool West",
                Title = "Sculptors Diary",
                popularity = "2342",
                likes = "4738",
                followed = "136",
                ranking = "5"
            ),
        )

        val rcv = view.findViewById<RecyclerView>(R.id.rcv)
        rcv?.adapter = myAdapter(requireContext(), profiles)

    }



    }

