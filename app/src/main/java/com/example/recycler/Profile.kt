package com.example.recycler

data class Profile
    (
    val image : Int,
    val Name: String,
    val Title: String,
    val popularity: String,
    val likes: String,
    val followed: String,
    val ranking : String

            )
