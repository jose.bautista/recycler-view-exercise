package com.example.recycler

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class myAdapter(val context: Context, private val profile: List<Profile>):RecyclerView.Adapter<myAdapter.myViewHolder>()
{
    inner class myViewHolder(view:View):RecyclerView.ViewHolder(view){

        val ranking = view.findViewById<TextView>(R.id.rankingValue)
        val likes = view.findViewById<TextView>(R.id.textViewLikesValue)
        val followed = view.findViewById<TextView>(R.id.textViewFollowedValue)
        val popularity = view.findViewById<TextView>(R.id.textViewPopularityValue)
        val Name = view.findViewById<TextView>(R.id.textViewName)
        val title = view.findViewById<TextView>(R.id.textViewTitle)
        private val root by lazy {view.rootView}
        val image = view.findViewById<ImageView>(R.id.imageView2)

        fun bind(profile: Profile){
            ranking.text = profile.ranking
            likes.text = profile.likes
            followed.text = profile.followed
            popularity.text = profile.popularity
            Name.text = profile.Name
            title.text = profile.Title
            image.setImageResource(profile.image)


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)

        return myViewHolder(view)
    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        holder.bind(profile[position])
    }

    override fun getItemCount(): Int {
        return profile.size
    }

}